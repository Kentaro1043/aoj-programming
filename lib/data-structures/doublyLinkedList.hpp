/**
 * @brief 連結リスト
 */
class list_lld
{
private:
    /**
     * @brief ノードの構造体
     */
    struct node
    {
        long long key;
        node *prev, *next;
    };
    /**
     * @brief 番兵のノード
     */
    node *nil;
public:
    /**
     * @brief コンストラクタ
     */
    list_lld()
    {
        nil = new node;
        nil->next = nil;
        nil->prev = nil;
    }
    /**
     * @brief ノードを先頭に挿入
     * 
     * @param key 挿入するノードのキー
     * @details O(1)
     */
    void insert(long long key)
    {
        node *x = new node;
        x->key = key;
        x->next = nil->next;
        nil->next->prev = x;
        nil->next = x;
        x->prev = nil;
    }
    /**
     * @brief ノードを探索
     * 
     * @param key 探索するキー
     * @return node* 見つかったノード(見つからなかった場合nil)
     * @details N個の要素を含むリストにおいてO(N)
     */
    node* listSearch(long long key)
    {
        node *cur = nil->next;
        while (cur != nil && cur->key != key)
        {
            cur = cur->next;
        }
        return cur;
    }
    /**
     * @brief 要素の削除
     * 
     * @param t 削除するノード
     */
    void deleteNode(node *t)
    {
        if (t == nil) return;
        t->prev->next = t->next;
        t->next->prev = t->prev;
        delete t;
    }
    /**
     * @brief 先頭の要素を削除
     * 
     * @details O(1)
     */
    void deleteFirst()
    {
        deleteNode(nil->next);
    }
    /**
     * @brief 末尾の要素を削除
     * 
     * @details O(1)
     */
    void deleteLast()
    {
        deleteNode(nil->prev);
    }
    /**
     * @brief キーで削除
     * 
     * @param key 削除するノードのキー
     * @details N個の要素を含むリストにおいてO(N)
     */
    void deleteKey(long long key)
    {
        deleteNode(listSearch(key));
    }
};
