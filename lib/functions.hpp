#include <bits/stdc++.h>
using namespace std;

//角度とラジアンの相互変換
//#include <cmath>
long double degreeToRadian(long double degree)
{
    return degree * M_PI / 180;
}
long double radianToDegree(long double radian)
{
    return radian * 180 / M_PI;
}

//各桁の和を求める
long long digitSum(long long n)
{
    long long sum = 0;
    while (n > 0)
    {
        sum += n % 10;
        n /= 10;
    }
    return sum;
}

//各桁の数字をvectorに格納
vector<long long> digitVector(long long n)
{
    vector<long long> v;
    while (n > 0)
    {
        v.push_back(n % 10);
        n /= 10;
    }
    return v;
}

//桁数を求める
long long digitLength(long long n)
{
    long long length = 0;
    while (n > 0)
    {
        length++;
        n /= 10;
    }
    return length;
}

//約数をvectorに格納
//O(√N)
vector<long long> getDivisor(long long n)
{
    vector<long long> divisor;
    for (long long i = 1; i * i <= n; i++)
    {
        if (n % i == 0)
        {
            divisor.push_back(i);
            if (i * i != n)
            {
                divisor.push_back(n / i);
            }
        }
    }
    sort(divisor.begin(), divisor.end());
    return divisor;
}
