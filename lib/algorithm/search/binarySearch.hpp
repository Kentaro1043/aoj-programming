/**
 * @brief 二分探索
 * 
 * @param a 探索する配列
 * @param n 配列aのサイズ
 * @param key 探索する値
 * @return true 存在する
 * @return false 存在しない
 */
bool binarySearch_lld(long long a[], long long n, long long key)
{
    long long left = 0, right = n;
    while (left < right)
    {
        long long mid = (left + right) / 2;
        if (a[mid] == key)
        {
            return true;
        }
        else if (key < a[mid])
        {
            right = mid;
        }
        else
        {
            left = mid + 1;    
        }
    }
    return false;
}
