/**
 * @brief 線形探索
 * 
 * @param a 探索する配列(サイズn+1)
 * @param n 配列aのサイズ
 * @param key 探索する値
 * @return long long 見つかった値のインデックス(見つからなければ-1)
 */
long long linearSearch_lld(long long a[], long long n, long long key)
{
    long long i = 0;
    a[n] = key;
    while (a[i] != key)
    {
        i++;
        if (i == n)
        {
            return -1;
        }
    }
    return i;
}
