//最大公約数
//O(log b)
long long gcd_lld(long long x, long long y)
{
    if (x < y)
    {
        long long t = x;
        x = y;
        y = t;
    }
    while (y > 0)
    {
        long long r = x % y;
        x = y;
        y = r;
    }
    return x;
}
