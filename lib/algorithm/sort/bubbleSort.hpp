/**
 * @brief バブルソート
 * 
 * @param a ソートする配列
 * @param n 配列のサイズ
 * @details O(n^2)、安定なソート
 */
void bubbleSort(long long a[], long long n)
{
    long long flag = 1;
    while (flag)
    {
        flag = 0;
        long long i = 0;
        for (long long j = n - 1; j >= i+1; j--)
        {
            if (a[j] < a[j-1])
            {
                long long t = a[j];
                a[j] = a[j-1];
                a[j-1] = t;
                flag = 1;
            }
        }
        i++;
    }
}
