/**
 * @brief 選択ソート
 * 
 * @param a ソートする配列
 * @param n 配列のサイズ
 * @details O(n^2)、不安定なソート
 */
void selectionSort(long long a[], long long n)
{
    for (long long i = 0; i < n; i++)
    {
        long long minj = i;
        for (long long j = i; j < n; j++)
        {
            if (a[j] < a[minj])
            {
                minj = j;
            }
        }
        long long t = a[i];
        a[i] = a[minj];
        a[minj] = t;
    }
}
