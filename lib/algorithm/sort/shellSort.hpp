/**
 * @brief シェルソート
 * 
 * @param a ソートする配列
 * @param n 配列のサイズ
 * @details O(n^1.25)が予測される
 */
#include <vector>
void shellSort(long long a[], long long n)
{
    std::vector<long long> g;
    int i = 0;
    for (int h = 1;;)
    {
        if (h > n) break;
        g.push_back(h);
        h = 3*h+1;
        i++;
    }
    for (int i = g.size()-1; i >= 0; i--)
    {
        insertionSortForShellSort(a, n, g[i]);
    }
}
/**
 * @brief シェルソート用挿入ソート
 * 
 * @param a ソートする配列
 * @param n 配列のサイズ
 * @details O(n^2)、安定なソート
 */
void insertionSortForShellSort(long long a[], long long n, long long g)
{
    for (int i = g; i <= n - 1; i++)
    {
        long long v = a[i];
        long long j = i - g;
        while(j >= 0 && a[j] > v)
        {
            a[j+g] = a[j];
            j-=g;
        }
        a[j+g] = v;
    }
}
