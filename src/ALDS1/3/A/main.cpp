#include <bits/stdc++.h>
typedef long long ll;
#define rep(i, n) for (int i = 0; i < (n); i++)
using namespace std;
/**
 * @brief スタック(long long専用)
 */
class stack_lld
{
public:
    /**
     * @brief 値の追加
     * 
     * @param arg 追加する値
     */
    void push(long long arg)
    {
        index++;
        n[index] = arg;
    }
    /**
     * @brief 値の削除
     * 
     * @return long long 削除した値 
     */
    long long pop()
    {
        if (index == -1)
        {
            return -1;
        }
        long long t = n[index];
        n[index] = -1;
        index--;
        return t;
    }
    /**
     * @brief 空かどうかの確認
     * 
     * @return true 空
     * @return false 空でない
     */
    bool isEmpty()
    {
        return index==-1;
    }
    /**
     * @brief 満杯かどうかの確認
     * 
     * @return true 満杯
     * @return false 満杯でない
     */
    bool isFull()
    {
        return index==200001;
    }
    /**
     * @brief 現在のサイズ
     * 
     * @return long long サイズ
     */
    long long size()
    {
        return index+1;
    }
private:
    /**
     * @brief 本体の配列
     */
    long long n[200001];
    /**
     * @brief 値が入っている最後のインデックス
     */
    int index = -1;
};
int main()
{
    stack_lld n;
    char t[100];
    while (scanf("%s", t) != EOF)
    {
        if (t[0] == '+')
        {
            int t1 = n.pop(), t2 = n.pop();
            n.push(t1+t2);
        }
        else if (t[0] == '-')
        {
            int t1 = n.pop(), t2 = n.pop();
            n.push(t2-t1);
        }
        else if (t[0] == '*')
        {
            int t1 = n.pop(), t2 = n.pop();
            n.push(t1*t2);
        }
        else
        {
            n.push(atoi(t));
        }
    }
    printf("%lld\n", n.pop());
    return 0;
}
