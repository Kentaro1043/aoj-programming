#include <bits/stdc++.h>
typedef long long ll;
#define rep(i, n) for (int i = 0; i < (n); i++)
using namespace std;
int main()
{
    int n;
    cin >> n;
    int a[n];
    rep(i, n) cin >> a[i];
    rep(i, n)
    {
        cout << a[i];
        if (i != n-1)
        {
            cout << " ";
        }
        else
        {
            cout << endl;
        } 
    }
    for (int i = 1; i <= n-1; i++)
    {
        int v = a[i];
        int j = i - 1;
        while (j >= 0 && a[j] > v)
        {
            a[j+1] = a[j];
            j--;
        }
        a[j+1] = v;
        rep (i, n)
        {
            cout << a[i];
            if (i != n-1)
            {
                cout << " ";
            }
            else
            {
                cout << endl;
            }
        }
    }
    return 0;
}

