#include <bits/stdc++.h>
#define rep(i, n) for (int i = 0; i < (int)(n); i++)
#define all(x) (x).begin(), (x).end()
typedef long long ll;
using namespace std;
int n;
vector<int> a;
bool solve(int i, int m)
{
    if (m == 0)
    {
        return true;
    }
    if (i >= n)
    {
        return false;
    }
    bool res = solve(i+1, m) || solve(i+1, m - a[i]);
    return res;
}
int main()
{
    cin >> n;
    rep(i, n)
    {
        int t; cin >> t;
        a.push_back(t);
    }
    int q;
    cin >> q;
    rep(i, q)
    {
        int t; cin >> t;
        if (solve(0, t))
        {
            cout << "yes" << endl;
        }
        else
        {
            cout << "no" << endl;
        }
    }
    return 0;
}
