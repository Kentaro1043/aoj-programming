#include <bits/stdc++.h>
#define rep(i, n) for (int i = 0; i < (n); ++i)
#define all(x) (x).begin(), (x).end()
using namespace std;
typedef long long ll;
/**
 * @brief 二分探索
 * 
 * @param a 探索する配列
 * @param n 配列aのサイズ
 * @param key 探索する値
 * @return true 存在する
 * @return false 存在しない
 */
bool binarySearch_lld(long long a[], long long n, long long key)
{
    long long left = 0, right = n;
    while (left < right)
    {
        long long mid = (left + right) / 2;
        if (a[mid] == key)
        {
            return true;
        }
        else if (key < a[mid])
        {
            right = mid;
        }
        else
        {
            left = mid + 1;    
        }
    }
    return false;
}
int main()
{
    int n; cin >> n;
    long long s[n];
    rep(i, n) cin >> s[i];
    int q; cin >> q;
    int ans = 0;
    rep(i, q)
    {
        long long t; cin >> t;
        if (binarySearch_lld(s, n, t))
        {
            ans++;
        }
    }
    cout << ans << endl;
    return 0;
}
