#include <bits/stdc++.h>
#define rep(i, n) for (int i = 0; i < (n); ++i)
#define all(x) (x).begin(), (x).end()
using namespace std;
typedef long long ll;
/**
 * @brief 線形探索
 * 
 * @param a 探索する配列(サイズn+1)
 * @param n 配列aのサイズ
 * @param key 探索する値
 * @return long long 見つかった値のインデックス(見つからなければ-1)
 */
long long linearSearch(long long a[], long long n, long long key)
{
    long long i = 0;
    a[n] = key;
    while (a[i] != key)
    {
        i++;
        if (i == n)
        {
            return -1;
        }
    }
    return i;
}

int main()
{
    long long n; scanf("%lld", &n);
    long long s[n+1];
    rep(i, n) scanf("%lld", &s[i]);
    long long q; scanf("%lld", &q);
    long long ans = 0;
    long long key;
    rep(i, q)
    {
        scanf("%lld", &key);
        if (linearSearch(s, n, key) != -1) ans++;
    }
    printf("%lld\n", ans);
    return 0;
}
