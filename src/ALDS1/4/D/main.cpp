#include <bits/stdc++.h>
#define rep(i, n) for (int i = 0; i < (int)(n); i++)
typedef long long ll;
using namespace std;
int main()
{
    int n, k; cin >> n >> k;
    vector<int> w(n);
    rep(i, n) cin >> w.at(i);
    ll left = 0, right = 100000 * 10000;
    ll mid;
    while (right - left > 1)
    {
        mid = (right + left) / 2;
        int v;
        int i = 0;
        bool chk = false;
        for (int j = 0; j < k; j++)
        {
            ll s = 0;
            while (s + w[i] <= mid)
            {
                s += w[i];
                i++;
                if (i == n) {v = n; chk = true; break;}
            }
            if (chk) break;
        }
        if (!chk) v = i;
        if (v >= n)
        {
            right = mid;
        }
        else
        {
            left = mid;
        }
    }
    cout << right << endl;
    return 0;
}
