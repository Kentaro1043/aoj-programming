#include <bits/stdc++.h>
typedef long long ll;
#define rep(i, n) for (int i = 0; i < (n); i++)
using namespace std;
int main()
{
    int n;
    cin >> n;
    int a[n];
    rep (i, n) cin >> a[i];
    int cnt = 0;
    int flag = 1;
    while (flag)
    {
        flag =0;
        for (int j = n-1; j >= 1; j--)
        {
            if (a[j] < a[j-1])
            {
                swap(a[j], a[j-1]);
                flag = 1;
                cnt++;
            }
        }
    }
    rep(i, n)
    {
        cout << a[i];
        if (i != n-1)
        {
            cout << " ";
        }
        else
        {
            cout << endl;
        }
    }
    cout << cnt << endl;
    return 0;
}

