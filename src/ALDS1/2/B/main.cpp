#include <bits/stdc++.h>
using namespace std;
#define rep(i, n) for (int i = 0; i < (int)(n); i++)
typedef long long ll;
int selectionSort(long long a[], long long n)
{
    int t = 0;
    for (long long i = 0; i < n; i++)
    {
        long long minj = i;
        for (long long j = i; j < n; j++)
        {
            if (a[j] < a[minj])
            {
                minj = j;
            }
        }
        swap(a[i], a[minj]);
        if (a[i] != a[minj]) t++;
    }
    return t;
}
int main()
{
    int n;
    cin >> n;
    ll a[n];
    rep(i, n) cin >> a[i];
    int t = selectionSort(a, n);
    rep(i, n)
    {
        cout << a[i];
        if (i != n-1) cout << " ";
    }
    cout << endl << t << endl;
    return 0;
}
