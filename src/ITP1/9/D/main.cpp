#include <iostream>
#include <string>
#include <algorithm>
using namespace std;
int main()
{
    string str;
    cin >> str;
    int q;
    cin >> q;

    string t1, t2;
    int a, b;
    for (int i = 0; i < q; i++)
    {
        cin >> t1 >> a >> b;
        if (t1 == "replace")
        {
            cin >> t2;
        }
        if (t1 == "print")
        {
            cout << str.substr(a, b - a + 1) << endl;
        }
        else if (t1 == "reverse")
        {
            reverse(str.begin() + a, str.begin() + b);
        }
        else
        {
            str.replace(a, t2.size(), t2);
        }
    }
    return 0;
}
