#include <iostream>
#include <string>
using namespace std;
int main()
{
    while (true)
    {
        string s;
        cin >> s;
        if (s == "-")
        {
            break;
        }
        int m;
        cin >> m;
        string t;
        int h;
        for (int i = 0; i < m; i++)
        {
            cin >> h;
            t = s.substr(0, h);
            s.erase(s.begin(), s.begin() + h);
            s += t;
        }
        cout << s << endl;
    }
    return 0;
}
