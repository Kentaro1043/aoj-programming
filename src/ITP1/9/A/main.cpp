#include <iostream>
#include <algorithm>
using namespace std;
int main()
{
    string w;
    cin >> w;
    string p;
    int ans = 0;
    while (cin >> p)
    {
        transform(p.begin(), p.end(), p.begin(), ::tolower);
        if (p == "END_OF_TEXT")
        {
            break;
        }
        else if (p == w)
        {
            ans++;
        }
    }
    cout << ans << endl;
    return 0;
}
