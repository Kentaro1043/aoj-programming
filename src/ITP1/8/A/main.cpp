#include <iostream>
#include <string>
#include <algorithm>
using namespace std;
int main()
{
    string s;
    getline(cin, s);
    string r;
    for (int i = 0; i < s.size(); i++)
    {
        if (s[i] == ' ')
        {
            r += ' ';
        }
        else if (isupper(s[i]))
        {
            r += tolower(s[i]);
        }
        else if (islower(s[i]))
        {
            r += toupper(s[i]);
        }
        else
        {
            r += s[i];
        }
    }
    cout << r << endl;
    return 0;
}
