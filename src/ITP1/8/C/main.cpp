#include <iostream>
#include <string>
#include <algorithm>
using namespace std;
int main()
{
    char c[1200];
    string s;
    while (cin >> c)
    {
        s += c;
    }
    int count[26];
    fill(count, count + 26, 0);
    transform(s.begin(), s.end(), s.begin(), ::tolower);
    for (int i = 0; i < s.size(); i++)
    {
        int t = s[i] - 'a';
        count[t]++;
    }
    cout << "a : " << count[0] << endl;
    cout << "b : " << count[1] << endl;
    cout << "c : " << count[2] << endl;
    cout << "d : " << count[3] << endl;
    cout << "e : " << count[4] << endl;
    cout << "f : " << count[5] << endl;
    cout << "g : " << count[6] << endl;
    cout << "h : " << count[7] << endl;
    cout << "i : " << count[8] << endl;
    cout << "j : " << count[9] << endl;
    cout << "k : " << count[10] << endl;
    cout << "l : " << count[11] << endl;
    cout << "m : " << count[12] << endl;
    cout << "n : " << count[13] << endl;
    cout << "o : " << count[14] << endl;
    cout << "p : " << count[15] << endl;
    cout << "q : " << count[16] << endl;
    cout << "r : " << count[17] << endl;
    cout << "s : " << count[18] << endl;
    cout << "t : " << count[19] << endl;
    cout << "u : " << count[20] << endl;
    cout << "v : " << count[21] << endl;
    cout << "w : " << count[22] << endl;
    cout << "x : " << count[23] << endl;
    cout << "y : " << count[24] << endl;
    cout << "z : " << count[25] << endl;
    return 0;
}
