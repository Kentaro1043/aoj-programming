#include <iostream>
#include <vector>
#include <string>
using namespace std;
int main()
{
    vector<string> v;
    string s;
    while(true)
    {
        cin >> s;
        if(s == "0")
        {
            break;
        }
        v.push_back(s);
    }
    int t = 0;
    for(int i = 0; i < v.size(); i++)
    {
        for(int j = 0; j < v[i].size(); j++)
        {
            t += v[i][j] - '0';
        }
        cout << t << endl;
        t = 0;
    }
    return 0;
}
