#include <iostream>
using namespace std;
int main()
{
    int r, c;
    cin >> r >> c;
    int a[r+1][c+1];
    for (int i = 0; i < r; i++)
    {
        for (int j = 0; j < c; j++)
        {
            cin >> a[i][j];
        }
    }

    int t = 0;
    for (int i = 0; i < r; i++)
    {
        for (int j = 0; j < c; j++)
        {
            t += a[i][j];
        }
        a[i][c] = t;
        t = 0;
    }
    for (int i = 0; i < c; i++)
    {
        for (int j = 0; j < r; j++)
        {
            t += a[j][i];
        }
        a[r][i] = t;
        t = 0;
    }
    for (int i = 0; i < r; i++)
    {
        t += a[i][c];
    }
    a[r][c] = t;

    for (int i = 0; i <= r; i++)
    {
        for (int j = 0; j <= c; j++)
        {
            cout << a[i][j];
            if (j != c)
            {
                cout << " ";
            }
        }
        cout << endl;
    }
    return 0;
}
