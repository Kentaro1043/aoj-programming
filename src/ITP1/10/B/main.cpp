#include <iostream>
#include <iomanip>
#include <math.h>
using namespace std;
int main()
{
    float a, b, c;
    cin >> a >> b >> c;
    c = (c / 360) * 2 * M_PI;
    cout << fixed << setprecision(10) << 0.5 * a * b * sin(c) << endl << a + b + sqrt(pow(a, 2) + pow(b, 2) - 2 * a * b * cos(c)) << endl << 0.5 * a * b * sin(c) * 2 / a << endl;
    return 0;
}
