#include <iostream>
#include <math.h>
#include <iomanip>
using namespace std;
int main()
{
    double x1, y1, x2, y2;
    cin >> x1 >> y1 >> x2 >> y2;
    cout << fixed << setprecision(10) << hypot(fabs(x2-x1), fabs(y2-y1)) << endl;
    return 0;
}
