#include <iostream>
#include <vector>
#include <string>
using namespace std;
int main()
{
    vector<int> a, b;
    vector<string> op;
    int t1,t3;
    string t2;
    int count = 0;
    while(true)
    {
        cin >> t1 >> t2 >> t3;
        if (t2 == "?")
        {
            break;
        }
        a.push_back(t1);
        op.push_back(t2);
        b.push_back(t3);
        count++;
    }
    for (int i = 0; i < count; i++)
    {
        if (op[i] == "+")
        {
            cout << a[i] + b[i] << endl;
        }
        else if(op[i] == "-")
        {
            cout << a[i] - b[i] << endl;
        }
        else if(op[i] == "*")
        {
            cout << a[i] * b[i] << endl;
        }
        else if(op[i] == "/")
        {
            cout << a[i] / b[i] << endl;
        }
    }
    return 0;
}
