#include <iostream>
#include <iomanip>
#include <math.h>
using namespace std;
int main()
{
    double r;
    cin >> r;
    cout << fixed << setprecision(8) << r*r*M_PI << " " << r*2*M_PI << endl;
    return 0;
}
