#include <iostream>
#include <algorithm>
using namespace std;
int main()
{
    int n;
    cin >> n;
    int a[n];
    for (int i = 0; i < n; i++)
    {
        cin >> a[i];
    }
    sort(a,a+n);
    long long sum = 0;

    for (int i = 0; i < n; i++)
    {
        sum += a[i];
    }
    cout << a[0] << " " << a[n-1] << " " << sum << endl;
    return 0;
}
