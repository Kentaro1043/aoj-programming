#include <iostream>
#include <string>
using namespace std;
class Dice
{
    private:
        int a=1, b=2, c=3, d=4, e=5,f=6;
    public:
        Dice(int n, int o, int p, int q, int r, int s)
        {
            a = n;
            b = o;
            c = p;
            d = q;
            e = r;
            f = s;
        }
        int getNumber(string arg)
        {
            int t;
            if (arg == "a")
            {
                t = a;
            }
            else if (arg == "b")
            {
                t = b;
            }
            else if (arg == "c")
            {
                t = c;
            }
            else if (arg == "d")
            {
                t = d;
            }
            else if (arg == "e")
            {
                t = e;
            }
            else if (arg == "f")
            {
                t = f;
            }
            return t;
        }
        void turn(string arg)
        {
            int beforeA = a, beforeB = b, beforeC = c, beforeD = d, beforeE = e, beforeF = f;
            if (arg == "N")
            {
                a = beforeB;
                b = beforeF;
                f = beforeE;
                e = beforeA;
            }
            else if (arg == "W")
            {
                a = beforeC;
                c = beforeF;
                f = beforeD;
                d = beforeA;
            }
            else if (arg == "E")
            {
                a = beforeD;
                c = beforeA;
                f = beforeC;
                d = beforeF;
            }
            else if (arg == "S")
            {
                a = beforeE;
                b = beforeA;
                f = beforeB;
                e = beforeF;
            }
        }
        bool isSame (Dice dice)
        {
            bool same = false;
            for (int i = 0; i < 6; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    if (a == dice.getNumber("a") && b == dice.getNumber("b") && c == dice.getNumber("c") && d == dice.getNumber("d") && e == dice.getNumber("e") && f == dice.getNumber("f"))
                    {
                        same = true;
                        break;
                    }
                    dice.turn("N");
                    dice.turn("W");
                    dice.turn("S");
                }
                if (same)
                {
                    break;
                }
                if (i == 0)
                {
                    dice.turn("N");
                }
                else if (i == 1)
                {
                    dice.turn("W");
                }
                else if (i == 2)
                {
                    dice.turn("N");
                    dice.turn("N");
                }
                else if (i == 3)
                {
                    dice.turn("W");
                }
                else if (i == 4)
                {
                    dice.turn("S");
                }
            }
            return same;
        }
};

int main()
{
    bool same = true;
    int n;
    cin >> n;
    int a, b, c, d, e, f;
    cin >> a >> b >> c >> d >> e >> f;
    Dice dice(a, b, c, d, e, f);
    for (int i = 1; i < n; i++)
    {
        int a, b, c, d, e, f;
        cin >> a >> b >> c >> d >> e >> f;
        Dice tmpDice(a, b, c, d, e, f);
        if (dice.isSame(tmpDice))
        {
            same = false;
        }
    }
    if (same)
    {
        cout << "Yes" << endl;
    }
    else
    {
        cout << "No" << endl;
    }
    return 0;
}
