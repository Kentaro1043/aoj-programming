#include <iostream>
#include <string>
using namespace std;

class Dice
{
    private:
        int a=1, b=2, c=3, d=4, e=5,f=6;
    public:
        Dice(int n, int o, int p, int q, int r, int s)
        {
            a = n;
            b = o;
            c = p;
            d = q;
            e = r;
            f = s;
        }
        int getNumber(int arg)
        {
            int t;
            if (arg == 1)
            {
                t = a;
            }
            else if (arg == 2)
            {
                t = b;
            }
            else if (arg == 3)
            {
                t = c;
            }
            else if (arg == 4)
            {
                t = d;
            }
            else if (arg == 5)
            {
                t = e;
            }
            else if (arg == 6)
            {
                t = f;
            }
            return t;
        }
        void turn(string arg)
        {
            int beforeA = a, beforeB = b, beforeC = c, beforeD = d, beforeE = e, beforeF = f;
            if (arg == "N")
            {
                a = beforeB;
                b = beforeF;
                f = beforeE;
                e = beforeA;
            }
            else if (arg == "W")
            {
                a = beforeC;
                c = beforeF;
                f = beforeD;
                d = beforeA;
            }
            else if (arg == "E")
            {
                a = beforeD;
                c = beforeA;
                f = beforeC;
                d = beforeF;
            }
            else if (arg == "S")
            {
                a = beforeE;
                b = beforeA;
                f = beforeB;
                e = beforeF;
            }
        }
};

int main()
{
    int a, b, c, d, e, f;
    cin >> a >> b >> c >> d >> e >> f;
    Dice dice(a, b, c, d, e, f);
    string s;
    cin >> s;
    for (int i = 0; i < s.size(); i++)
    {
        dice.turn(s.substr(i, 1));
    }
    cout << dice.getNumber(1) << endl;
    return 0;
}
